/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2023 Intel Corporation
 */

#include "intel_wakeref.h"

static inline void disable_rpm_wakeref_asserts(void *rpm)
{
}

static inline void enable_rpm_wakeref_asserts(void *rpm)
{
}
